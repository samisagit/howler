Howler
======

[![pipeline status](https://gitlab.com/samisagit/howler/badges/master/pipeline.svg)](https://gitlab.com/samisagit/howler/commits/master)
[![coverage report](https://gitlab.com/samisagit/howler/badges/master/coverage.svg)](https://gitlab.com/samisagit/howler/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/samisagit/howler?status.svg)](https://godoc.org/gitlab.com/samisagit/howler)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/samisagit/howler)](https://goreportcard.com/report/gitlab.com/samisagit/howler)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Howler makes errors easier to deal with, as well as exposing either an error code, or (for non production envs) the actual error thrown.

Installation
------------

Install Howler using the "go get" command:

    go get gitlab.com/samisagit/howler

Howler relies on std lib only, but requires Go version >= 1.12.

Documentation
------------

Refer to https://godoc.org/gitlab.com/samisagit/howler for the exposed API reference.

```go
h := howler.CannotReadRequestBody
errValue := h.Error() // 2365: cannot read request body

// Howlers are muzzled by default - the message will be omitted when marshalling
var b []byte
var marshalledErr string
b, _ = json.Marshal(h)
marshalledErr = string(b) // {"code": 2365}

// UnMuzzle() allows message to be included when marshalling
b, _ = json.Marshal(h.UnMuzzle())
marshalledErr = string(b) // {"code": 2365, "message": "cannot read request body"}

// note - the call to UnMuzzle() above does not mutate h, a new Howler is returned
// further marshalling to h will be muzzled
b, _ = json.Marshal(h)
marshalledErr = string(b) // {"code": 2365}

// if an unmuzzled instance is needed
uh := h.UnMuzzle()
b, _ = json.Marshal(uh)
marshalledErr = string(b) // {"code": 2365, "message": "cannot read request body"}

// in the unlikely event an unmuzzled Howler needs to be muzzled again
mh := uh.Muzzle()
b, _ = json.Marshal(mh)
marshalledErr = string(b) // {"code": 2365}
```

License
-------

Howler is available under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).