package howler

import (
	"fmt"
	"io"
)

// Howler can be used in place of errors, and JSON marshalled for HTTP responses
// to document what went wrong, whilst obscuring the details
type Howler struct {
	Code    int    `json:"code"`
	Message string `json:"message,omitempty"`
	message string
}

// Error will return the Howler Code and message fields, used to implement the
// error interface
func (h Howler) Error() string {
	return fmt.Sprintf("%d: %s", h.Code, h.message)
}

// UnMuzzle returns a new Howler with the Message field populated by the contents
// of the message field, thus allowing JSON marshal to include the message
func (h Howler) UnMuzzle() Howler {
	h.Message = h.message
	return h
}

// Muzzle returns a new Howler with the Message field emptied, causing the message
// JSON key to be omitted
func (h Howler) Muzzle() Howler {
	h.Message = ""
	return h
}

// Howl calls WriteString on the StringWriter passed to it, using the Howler.Error()
// method as the string
func (h Howler) Howl(w io.StringWriter) (int, error) {
	return w.WriteString(h.Error())
}

// input errors
var (
	CannotReadRequestBody      = Howler{Code: 2365, message: "cannot read request body"}
	ParameterValuesNotProvided = Howler{Code: 3017, message: "parameter values not provided"}
)

// data fetch errors
var (
	DataNotPresentPersistentStorage = Howler{Code: 4077, message: "data not present (persistent storage)"}
	DataNotPresentMessenger         = Howler{Code: 5014, message: "data not present (messenger)"}
)

// connection errors
var (
	ConnectionErrorPersistentStorage = Howler{Code: 2120, message: "connection error (persistent storage)"}
	ConnectionErrorMessenger         = Howler{Code: 4575, message: "connection error (messenger)"}
)
