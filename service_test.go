package howler

import (
	"fmt"
	"testing"
)

func TestNew(t *testing.T) {
	cases := []struct {
		name    string
		code    int
		message string
	}{
		{
			name:    "happy path",
			code:    10,
			message: "test error",
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			h := New(val.code, val.message)

			if h.Message != "" {
				t.Errorf("expected Message field to be empty")
			}

			if h.Error() != fmt.Sprintf("%d: %s", val.code, val.message) {
				t.Errorf(
					"expected Error() value to be %s, got %s",
					fmt.Sprintf("%d: %s", val.code, val.message),
					h.Error(),
				)

			}
		})
	}
}
