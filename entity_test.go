package howler

import (
	"fmt"
	"io"
	"log"
	"testing"
)

func TestError(t *testing.T) {
	cases := []struct {
		name           string
		howler         Howler
		expectedOutput string
	}{
		{
			name: "happy path",
			howler: Howler{
				Code:    1,
				message: "test message",
				Message: "test message",
			},
			expectedOutput: fmt.Sprintf("%d: %s", 1, "test message"),
		},
		{
			name: "no exported message",
			howler: Howler{
				Code:    1,
				message: "test message",
			},
			expectedOutput: fmt.Sprintf("%d: %s", 1, "test message"),
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			result := val.howler.Error()
			if result != val.expectedOutput {
				t.Errorf("expected output to be %s, got %s", val.expectedOutput, result)
			}
		})
	}
}

func TestUnMuzzle(t *testing.T) {
	cases := []struct {
		name           string
		howler         Howler
		expectedOutput Howler
	}{
		{
			name: "happy path",
			howler: Howler{
				Code:    1,
				message: "test message",
			},
			expectedOutput: Howler{
				Code:    1,
				message: "test message",
				Message: "test message",
			},
		},
		{
			name: "Message present",
			howler: Howler{
				Code:    1,
				message: "test message",
				Message: "test message",
			},
			expectedOutput: Howler{
				Code:    1,
				message: "test message",
				Message: "test message",
			},
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			result := val.howler.UnMuzzle()

			if result.Code != val.expectedOutput.Code {
				t.Errorf("expected output to have Code %d, got %d", val.expectedOutput.Code, result.Code)
			}

			if result.message != val.expectedOutput.message {
				t.Errorf("expected output to have message %s, got %s", val.expectedOutput.message, result.message)
			}

			if result.Message != val.expectedOutput.Message {
				t.Errorf("expected output to have Message %s, got %s", val.expectedOutput.Message, result.Message)
			}

		})
	}
}

func TestMuzzle(t *testing.T) {
	cases := []struct {
		name           string
		howler         Howler
		expectedOutput Howler
	}{
		{
			name: "happy path",
			howler: Howler{
				Code:    1,
				message: "test message",
			},
			expectedOutput: Howler{
				Code:    1,
				message: "test message",
			},
		},
		{
			name: "Message present",
			howler: Howler{
				Code:    1,
				message: "test message",
				Message: "test message",
			},
			expectedOutput: Howler{
				Code:    1,
				message: "test message",
			},
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			result := val.howler.Muzzle()

			if result.Code != val.expectedOutput.Code {
				t.Errorf("expected output to have Code %d, got %d", val.expectedOutput.Code, result.Code)
			}

			if result.message != val.expectedOutput.message {
				t.Errorf("expected output to have message %s, got %s", val.expectedOutput.message, result.message)
			}

			if result.Message != val.expectedOutput.Message {
				t.Errorf("expected output to have Message %s, got %s", val.expectedOutput.Message, result.Message)
			}

		})
	}
}

type ReadStringWriter interface {
	io.Reader
	io.StringWriter
}

type testReadStringWriter struct {
	store  []byte
	cursor *struct {
		i int
	}
}

func (t testReadStringWriter) Read(p []byte) (int, error) {
	finalDestination := len(p) - 1
	i := 0
	for _, val := range t.store {
		p[i] = val
		if i == finalDestination {
			return i, fmt.Errorf("Reader ran out of content")
		}
		if i == t.cursor.i {
			log.Println("hit cursor, quitting")
			return i, nil
		}
		i++
	}

	return i, nil
}

func (t testReadStringWriter) WriteString(p string) (int, error) {
	finalDestination := len(t.store) - 1
	i := 0
	for _, val := range []byte(p) {
		if t.cursor.i > finalDestination {
			return i, fmt.Errorf("Writer ran out of space")
		}
		t.store[t.cursor.i] = val
		t.cursor.i++
		i++
	}
	return i, nil
}

func TestHowl(t *testing.T) {
	cases := []struct {
		name            string
		h               Howler
		inputReadWriter ReadStringWriter
		storeSize       int
		errorExpected   bool
	}{
		{
			name: "happy path",
			h: Howler{
				Code:    1,
				message: "test message",
			},
			inputReadWriter: testReadStringWriter{
				store:  make([]byte, 15),
				cursor: &struct{ i int }{},
			},
			storeSize:     15,
			errorExpected: false,
		},
		{
			name: "writer too small",
			h: Howler{
				Code:    1,
				message: "test message",
			},
			inputReadWriter: testReadStringWriter{
				store:  make([]byte, 13),
				cursor: &struct{ i int }{},
			},
			storeSize:     13,
			errorExpected: true,
		},
	}

	for _, val := range cases {
		t.Run(val.name, func(t *testing.T) {
			written, err := val.h.Howl(val.inputReadWriter)
			if err != nil {
				if val.errorExpected == false {
					t.Errorf("expected err to be nil, got error with message: %s", err.Error())
				}
				if err.Error() == "Writer ran out of space" {
					if written != val.storeSize {
						t.Errorf("expected to write %d bytes, only wrote %d", val.storeSize, written)
					}
				} else {
					t.Errorf("unexpected error with value: %s", err.Error())
				}
			}
			destination := make([]byte, 15)
			val.inputReadWriter.Read(destination)

			if string(destination) != fmt.Sprintf("%d: %s", val.h.Code, val.h.message) && !val.errorExpected {
				t.Errorf("expected destination to have value %v, got %v",
					[]byte(fmt.Sprintf("%d: %s", val.h.Code, val.h.message)),
					destination,
				)
			}
		})
	}
}
