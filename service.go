package howler

// New returns a muzzled Howler populated with the code and message provided
func New(code int, message string) Howler {
	return Howler{
		Code:    code,
		message: message,
	}
}
